﻿using UnityEngine;
using System.Collections;

public class Boss1Add : Enemy
{
    public float shootInterval = 100f;
    public float movementAngle = 0f;
    public float movementSpeed = 3f;

    [Header("Pattern")]
    public Bullet bulletPrefab;
    public int bulletCount = 3;
    public float bulletSpeed;

    private float currentShootInterval = 100f;

    protected override void Awake()
    {
        base.Awake();

        currentShootInterval = shootInterval;
    }

    protected override void Update()
    {
        base.Update();

        transform.position += (Vector3)MathEx.VectorFromAngle(movementAngle) * movementSpeed * Time.deltaTime;

        currentShootInterval -= Time.deltaTime;
        if (currentShootInterval <= 0f)
        {
            var angle = MathEx.AngleFromVector(Util.Player.transform.position - transform.position);
            var bullets = Pattern.RadialSpread(bulletCount, angle, 360f/bulletCount, bulletSpeed, 1, transform.position, bulletPrefab);

            foreach (var bullet in bullets)
            {
                bullet.rotateTowardsAngle = true;
                bullet.spriteRenderer.color = Color.green;
            }
        }

        if (!Util.stageBounds.Contains((Vector2)transform.position))
        {
            Destroy(gameObject);
        }
    }
}
