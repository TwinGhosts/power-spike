﻿using UnityEngine;

[System.Serializable]
public struct BossPhase
{
    public float Health;
    public float MaxHealth;
    public float InitialInvincibilityDuration;
    public Color32 Color;
}
