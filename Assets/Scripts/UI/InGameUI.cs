﻿using DG.Tweening;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    [SerializeField] private List<Image> playerLives = new List<Image>();
    [SerializeField] private Sprite playerLifeGraphic;
    [SerializeField] private Sprite playerLifeEmptyGraphic;
    [SerializeField] private Text goldText;
    [SerializeField] private Graphic whiteFlash;
    [SerializeField] private Transform bossHealthParent;
    [SerializeField] private Slider bossHealthPrefab;
    [SerializeField] private Player player;
    [SerializeField] private float barFillTime = 1f;

    private List<Slider> bossHealthbars = new List<Slider>();
    private bool isFlashing = false;
    private readonly Color visibleColor = new Color(255, 255, 255, 1f);
    private readonly Color invisibleColor = new Color(255, 255, 255, 0f);

    public Player Player => player;

    private void Awake()
    {
        Util.InGameUI = this;
    }

    public void SetPlayer(Player player)
    {
        this.player = player;

        player.OnHit += (damage) =>
        {
            for (int i = 0; i < playerLives.Count; i++)
            {
                if (player.ShipData.Health > i)
                {
                    playerLives[i].sprite = (player.ShipData.Health > i) ? playerLifeGraphic : playerLifeEmptyGraphic;
                    playerLives[i].color = visibleColor;
                }
                else
                    playerLives[i].color = invisibleColor;
            }
        };
    }

    public void SetBoss(Boss boss)
    {
        CreateHealthbars(boss);
        StartCoroutine(_FillHealthBar());

        boss.OnHit += (damage) => UpdateHealthbars(boss);
    }

    public void FlashScreen(float duration = 0.3f)
    {
        if (!isFlashing)
            StartCoroutine(_Flash(duration));
    }

    private void UpdateHealthbars(Boss boss)
    {
        var index = 0;
        foreach (var phase in boss.Phases)
        {
            if (index == boss.CurrentPhaseIndex)
            {
                bossHealthbars[index].value = boss.CurrentPhase.Health;
            }

            index++;
        }
    }

    private void CreateHealthbars(Boss boss)
    {
        var totalHealth = boss.TotalHealth;
        foreach (var phase in boss.Phases)
        {
            var bar = Instantiate(bossHealthPrefab, bossHealthParent);
            var colors = bar.colors;
            colors.disabledColor = phase.Color;
            bar.colors = colors;
            bar.minValue = 0f;
            bar.maxValue = phase.MaxHealth;
            bar.value = phase.Health;
            var layoutElement = bar.GetComponent<LayoutElement>();
            layoutElement.flexibleWidth = phase.MaxHealth / totalHealth;
            bossHealthbars.Add(bar);
        }
    }

    private IEnumerator _FillHealthBar()
    {
        var completed = false;
        var totalValue = 0f;

        bossHealthbars.ForEach((bar) =>
        {
            bar.value = 0f;
            totalValue += bar.maxValue;
        });

        foreach (var bar in bossHealthbars)
        {
            var fraction = bar.maxValue / totalValue;
            DOTween.To(() => bar.value, (value) => bar.value = value, bar.maxValue, barFillTime * fraction).OnComplete(() => completed = true);
            yield return new WaitUntil(() => completed);
            completed = false;
        }
    }

    private IEnumerator _Flash(float duration)
    {
        isFlashing = true;

        var startColor = whiteFlash.color;
        var endColor = startColor;
        endColor.a = 1f;
        var progress = 0f;

        while (progress < 1f)
        {
            progress += Time.deltaTime / (duration / 2f);
            whiteFlash.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }

        progress = 0f;

        while (progress < 1f)
        {
            progress += Time.deltaTime / (duration / 2f);
            whiteFlash.color = Color.Lerp(endColor, startColor, progress);
            yield return null;
        }

        isFlashing = false;
    }
}
