﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIUpgradeSection : MonoBehaviour
{
    [Header("Values")]
    public int value = 0;
    public UpgradeType type;

    [Header("References")]
    public Text title;
    public Text valueText;
    public Button upgradeButton;
    public Button downgradeButton;
    public List<Image> points;
    public List<int> costs;

    [Header("Graphics")]
    public Sprite pointFull;
    public Sprite pointEmpty;

    private int maxValue;

    private void Awake()
    {
        maxValue = points.Count;
        upgradeButton.onClick.AddListener(() => { Upgrade(); });
        downgradeButton.onClick.AddListener(() => { Downgrade(); });

        value = LoadStatProgress();
        UpdateGraphics();
        valueText.text = costs[value].ToString();
    }

    private void Update()
    {
        upgradeButton.interactable = (value != maxValue) && (costs[value] <= Stats.Gold);
        downgradeButton.interactable = value != 0;
    }

    private void UpdateGraphics()
    {
        for (var i = 0; i < points.Count; i++)
        {
            points[i].sprite = (value - 1 >= i) ? pointFull : pointEmpty;
        }

        valueText.text = costs[value].ToString();
    }

    public virtual void Upgrade()
    {
        Stats.Gold = Mathf.Clamp(Stats.Gold - costs[value], 0, int.MaxValue);
        value = Mathf.Clamp(value + 1, 0, maxValue);
        UpdateGraphics();
        UpdateStats();
    }

    public virtual void Downgrade()
    {
        Stats.Gold = Mathf.Clamp(Stats.Gold + costs[value] / 2, 0, int.MaxValue);
        value = Mathf.Clamp(value - 1, 0, maxValue);
        UpdateGraphics();
        UpdateStats();
    }

    private void UpdateStats()
    {
        switch (type)
        {
            case UpgradeType.Damage:
                Stats.uDamage = value;
                break;
            case UpgradeType.AttackSpeed:
                Stats.uAttackSpeed = value;
                break;
            case UpgradeType.AttackType:
                Stats.uAttackType = value;
                break;
            case UpgradeType.Speed:
                Stats.uMovementSpeed = value;
                break;
            case UpgradeType.Health:
                Stats.uHealth = value;
                break;
            case UpgradeType.Coins:
                Stats.uCoins = value;
                break;
            case UpgradeType.Magnet:
                Stats.uMagnetRange = value;
                break;
            case UpgradeType.Special:
                Stats.uSpecial = value;
                break;
        }
    }

    private int LoadStatProgress()
    {
        switch (type)
        {
            default:
            case UpgradeType.Damage:
                return Stats.uDamage;
            case UpgradeType.AttackSpeed:
                return Stats.uAttackSpeed;
            case UpgradeType.AttackType:
                return Stats.uAttackType;
            case UpgradeType.Speed:
                return Stats.uMovementSpeed;
            case UpgradeType.Health:
                return Stats.uHealth;
            case UpgradeType.Coins:
                return Stats.uCoins;
            case UpgradeType.Magnet:
                return Stats.uMagnetRange;
            case UpgradeType.Special:
                return Stats.uSpecial;
        }
    }
}

public enum UpgradeType
{
    Damage,
    AttackSpeed,
    AttackType,
    Speed,
    Health,
    Coins,
    Magnet,
    Special,
}