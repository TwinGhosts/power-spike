﻿using UnityEngine;
using UnityEngine.UI;

public class UIUpgradeScreen : MonoBehaviour
{
    public Button goButton;
    public Button menuButton;
    public Text goldText;

    private void Awake()
    {
        goldText.text = Stats.Gold.ToString();
        goButton.onClick.AddListener(() => { Util.FadeManager.GoToScene(Util.SCENE_GAME); });
        menuButton.onClick.AddListener(() => { Util.FadeManager.GoToScene(Util.SCENE_MAIN_MENU); });
    }

    private void Update()
    {
        if (Time.frameCount % 5 == 0)
            goldText.text = Stats.Gold.ToString();
    }
}
