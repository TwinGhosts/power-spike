﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    public float angle = 0f;
    public float speed = 0f;
    public float damage = 0f;
    public bool rotateTowardsAngle = false;

    private void Update()
    {
        transform.position += (Vector3)MathEx.VectorFromAngle(angle) * Time.deltaTime * speed;
        if (rotateTowardsAngle) transform.rotation = Quaternion.Euler(0f, 0f, angle + 90f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var enemy = collision.GetComponent<Enemy>();
        var boss = collision.GetComponent<Boss>();

        if (enemy)
        {
            enemy.Damage(damage);
            Destroy(gameObject);
        }
        else if (boss)
        {
            boss.Damage(damage);
            Destroy(gameObject);
        }
    }
}
