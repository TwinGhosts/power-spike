﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Title("Intro")]
    public Transform introPosition;
    public float introTime = 2f;

    [Title("State")]
    public State state = State.INTRO;

    [Title("Data")]
    [SerializeField] private ShipDataScriptable scriptableDataHolder;
    private ShipData shipData;
    private UpgradeValueData upgradeValueData;
    private UpgradeCountData upgradeCountData;

    [Title("References")]
    [SerializeField] private PlayerBullet bulletPrefab;
    [SerializeField] private ParticleSystem explosionSystem = null;
    [SerializeField] private ParticleSystem exhaustSystem = null;
    [SerializeField] private SpriteRenderer lightSprite = null;

    private SpriteRenderer spriteRenderer;
    private Rigidbody2D body;
    private float dieTime = 3f;

    public Action<float> OnHit = (damage) => { };
    public Action OnDeath = () => { };

    public ShipData ShipData => shipData;
    private UpgradeValueData UpgradeValueData => upgradeValueData;
    private UpgradeCountData UpgradeCountData => upgradeCountData;

    private void Awake()
    {
        Util.Player = this;

        shipData = scriptableDataHolder.ShipData;
        upgradeValueData = scriptableDataHolder.UpgradeData;

        shipData.Init();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        body = GetComponentInChildren<Rigidbody2D>();
    }

    private void Start()
    {
        Util.InGameUI.SetPlayer(this);
        StartCoroutine(Intro());
    }

    private IEnumerator Intro()
    {
        Vector2 startPos = transform.position;
        var progress = 0f;

        while (progress < 1f)
        {
            progress += Time.deltaTime / introTime;
            transform.position = Vector2.Lerp(startPos, introPosition.position, Mathf.SmoothStep(0f, 1f, progress));
            yield return null;
        }

        state = State.ACTIVE;
    }

    private void Update()
    {
        shipData.Update();

        if (Input.GetKeyDown(KeyCode.Space)) Damage(1);

        if (spriteRenderer.color != Color.white) spriteRenderer.color = Color.Lerp(spriteRenderer.color, Color.white, Time.deltaTime * 2f);

        switch (state)
        {
            case State.INTRO:
                break;
            case State.ACTIVE:
                ClampToStageBounds();
                Shoot();
                break;
            case State.DEAD:
                ClampToStageBounds();

                dieTime -= Time.deltaTime;

                var targetColor = Color.white;
                targetColor.a = 0f;
                spriteRenderer.color = Color.Lerp(spriteRenderer.color, targetColor, Time.deltaTime * 2f);

                if (dieTime < 0.1f && dieTime > 0f) Util.InGameUI.FlashScreen();
                if (dieTime <= 0f)
                {
                    spriteRenderer.sprite = null;
                    lightSprite.sprite = null;
                    exhaustSystem.Stop();
                }

                if (dieTime <= -1.5f)
                {
                    Util.FadeManager.GoToScene(Util.SCENE_UPGRADE);
                }
                break;
        }
    }

    private void LateUpdate()
    {
        if (state == State.ACTIVE)
            Movement();
    }

    private void Shoot()
    {
        if (!shipData.AttackIsOnCooldown && Input.GetKey(KeyCode.Z))
        {
            shipData.Attack();

            switch (upgradeCountData.UniqueOne)
            {
                case 0:
                    var b1_1 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f, Quaternion.identity);
                    b1_1.angle = 90f;
                    b1_1.damage = shipData.Damage;
                    b1_1.speed = 6f;
                    break;

                case 1:
                    var b2_1 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f + Vector2.left * 0.1f, Quaternion.identity);
                    var b2_2 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f + Vector2.right * 0.1f, Quaternion.identity);
                    b2_1.angle = b2_2.angle = 90f;
                    b2_1.damage = b2_2.damage = shipData.Damage;
                    b2_1.speed = b2_2.speed = 6.5f;
                    break;

                case 2:
                    var b3_1 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f + Vector2.left * 0.15f, Quaternion.identity);
                    var b3_2 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f + Vector2.right * 0.15f, Quaternion.identity);
                    var b3_3 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f, Quaternion.identity);
                    b3_1.angle = b3_2.angle = b3_3.angle = 90f;
                    b3_1.damage = b3_2.damage = b3_3.damage = shipData.Damage;
                    b3_1.speed = b3_2.speed = b3_3.speed = 7f;
                    break;

                case 3:
                    var b4_1 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f + Vector2.left * 0.15f, Quaternion.identity);
                    var b4_2 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f + Vector2.right * 0.15f, Quaternion.identity);
                    var b4_3 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f, Quaternion.identity);
                    var b4_4 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f + Vector2.left * 0.21f, Quaternion.identity);
                    var b4_5 = Instantiate(bulletPrefab, (Vector2)transform.position + Vector2.up * 0.25f + Vector2.right * 0.21f, Quaternion.identity);
                    b4_1.angle = b4_2.angle = b4_3.angle = 90f;
                    b4_4.angle = 105f;
                    b4_5.angle = 75f;
                    b4_1.damage = b4_2.damage = b4_3.damage = b4_4.damage = b4_5.damage = shipData.Damage;
                    b4_1.speed = b4_2.speed = b4_3.speed = 7.5f;
                    b4_4.speed = b4_5.speed = 6.5f;
                    b4_4.rotateTowardsAngle = b4_5.rotateTowardsAngle = true;
                    break;
            }
        }
    }

    private void Movement()
    {
        var input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if (input == Vector2.zero) return;

        transform.position += (Vector3)input * Time.deltaTime * shipData.MovementSpeed;
    }

    private void ClampToStageBounds()
    {
        var bounds = Util.OrthographicBounds(Camera.main);
        var pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, -bounds.size.x / 2f, bounds.size.x / 2f);
        pos.y = Mathf.Clamp(pos.y, -bounds.size.y / 2f, bounds.size.y / 2f);

        transform.position = pos;
    }

    public void Damage(int damage)
    {
        if (state != State.ACTIVE) return;

        shipData.DealDamage(damage);

        Util.CameraController.Shake(Vector2.one / 3f, 0.1f);

        spriteRenderer.color = Color.red;

        if (shipData.Damage <= 0)
        {
            state = State.DEAD;
            explosionSystem.gameObject.transform.SetParent(null);
            explosionSystem.Play();
            Util.CameraController.Shake(Vector2.one * 0.25f, dieTime);
        }
    }

    public enum State
    {
        INTRO,
        ACTIVE,
        DEAD,
    }
}
