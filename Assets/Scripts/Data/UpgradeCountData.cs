﻿[System.Serializable]
public struct UpgradeCountData
{
    public int Health;
    public int Damage;
    public int Magnet;

    public int MovementSpeed;
    public int AttackSpeed;

    public int UniqueOne;
    public int UniqueTwo;
    public int UniqueThree;
    public int UniqueFour;
}
