﻿using System.Collections.Generic;
using UnityEngine;

public class EntityManager
{
    public List<Bullet> HostileBullets { get; protected set; } = new List<Bullet>();
    public List<Bullet> PlayerBullets { get; protected set; } = new List<Bullet>();
    public List<Enemy> Enemies { get; protected set; } = new List<Enemy>();
    public List<Gold> Gold { get; protected set; } = new List<Gold>();

    public EntityManager()
    {
        Util.EntityManager = this;
    }

    public void KillAllHostileBullets()
    {
        for (var i = HostileBullets.Count - 1; i >= 0; i--)
        {
            var bullet = HostileBullets[i];
            HostileBullets.Remove(bullet);
            Object.Destroy(bullet.gameObject);
        }
    }
}
