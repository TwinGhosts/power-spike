﻿using System.Collections.Generic;

[System.Serializable]
public struct UpgradeValueData
{
    public List<int> Health;
    public List<int> Damage;
    public List<int> Magnet;

    public List<int> MovementSpeed;
    public List<int> AttackSpeed;

    public List<int> UniqueOne;
    public List<int> UniqueTwo;
    public List<int> UniqueThree;
    public List<int> UniqueFour;
}
