﻿using UnityEngine;

[CreateAssetMenu(fileName = "Ship Data", menuName = "ScriptableObjects/Create Ship Data", order = 1)]
public class ShipDataScriptable : ScriptableObject
{
    public ShipData ShipData;
    public UpgradeValueData UpgradeData;
}
