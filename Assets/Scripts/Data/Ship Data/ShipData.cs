﻿using Sirenix.OdinInspector;
using UnityEngine;

[System.Serializable]
public struct ShipData
{
    [Title("Descriptives")]
    public string Name;
    [TextArea] public string Description;

    [Title("General")]
    public int Health;
    public int MaxHealth;
    public float MagnetRange;
    public float MovementSpeed;
    public float AttackSpeed;
    public float Damage;

    [Title("Uniques")]
    public float UniqueOne;
    public float UniqueTwo;
    public float UniqueThree;
    public float UniqueFour;

    private float attackCooldown;

    public bool AttackIsOnCooldown => attackCooldown > 0f;

    public void Init()
    {
        attackCooldown = 0f;
    }

    public void Update()
    {
        attackCooldown -= Time.deltaTime;
    }

    public void Attack()
    {
        attackCooldown = AttackSpeed;
    }

    public void DealDamage(int damage)
    {
        Health -= damage;
        Health = Mathf.Clamp(Health, 0, 100);
    }
}
