﻿using UnityEngine;
using System.Collections.Generic;

public static class Pattern
{
    public static List<Bullet> RadialSpread(int amount, float startAngle, float width, float speed, int damage, Vector2 origin, Bullet prefab)
    {
        List<Bullet> bulletList = new List<Bullet>();
        
        for(var i = 0; i < amount; i++)
        {
            var bullet = Object.Instantiate(prefab, origin, Quaternion.identity);
            bullet.angle = startAngle;
            bullet.damage = damage;
            bullet.speed = speed;
            bulletList.Add(bullet);
            startAngle += width / amount;
        }

        return bulletList;
    }

    public static System.Collections.IEnumerator LineRadialDelay(int lineCount, int lineLength, float delay, float startAngle, float width, float speed, int damage, Vector2 origin, Bullet prefab)
    {
        var progress = 0;
        while (progress < lineLength)
        {
            for (var i = 0; i < lineCount; i++)
            {
                var bullet = Object.Instantiate(prefab, origin, Quaternion.identity);
                bullet.angle = startAngle;
                bullet.damage = damage;
                bullet.speed = speed;
                startAngle += width / lineCount;
            }

            progress++;
            yield return new WaitForSeconds(delay);
        }
    }
}