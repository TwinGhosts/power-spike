﻿using DG.Tweening;
using System;
using UnityEngine;

public class Gold : MonoBehaviour
{
    public int Value = 1;
    public bool MoveToPlayer = false;

    private bool magnet = false;
    private float speed = 0f;
    private float maxSpeed = 3f;

    public Action OnCollect = () => { };

    private void Start()
    {
        Util.EntityManager.Gold.Add(this);
    }

    private void Update()
    {
        if (MoveToPlayer) speed += Time.deltaTime * maxSpeed;

        if (magnet) speed += Time.deltaTime * maxSpeed;
        else if (speed > 0f && !MoveToPlayer) speed -= Time.deltaTime * maxSpeed;

        speed = Mathf.Clamp(speed, 0f, maxSpeed);

        if (speed > 0f)
        {
            transform.position += (Util.Player.transform.position - transform.position).normalized * speed * Time.deltaTime;
        }
    }

    public void Spread(Vector2 targetLocation)
    {
        transform.DOMove(targetLocation, 1f).SetEase(Ease.InOutCirc);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player)
        {
            Stats.Gold += Value;
            OnCollect.Invoke();
            Destroy(gameObject);
        }

        var coinMagnet = collision.GetComponent<CoinMagnet>();
        if (coinMagnet)
        {
            magnet = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        var coinMagnet = collision.GetComponent<CoinMagnet>();
        if (coinMagnet)
        {
            magnet = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var coinMagnet = collision.GetComponent<CoinMagnet>();
        if (coinMagnet)
        {
            magnet = false;
        }
    }

    private void OnDestroy()
    {
        Util.EntityManager.Gold.Remove(this);
    }
}
