﻿public static class Stats
{
    public static int Gold = 0;

    private static readonly float[] Damage = new float[] { 1f, 1.4f, 1.8f, 2.2f, 2.6f, 3.0f, 3.4f, 3.8f, 4.2f, 4.8f };
    private static readonly float[] MovementSpeed = new float[] { 3f, 3.4f, 3.8f, 4.2f, 4.6f, 5f, 5.4f, 5.7f, 6f, 6.3f, 6.6f };
    private static readonly int[] AttackType = { 0, 1, 2, 3, 4, 5 };
    private static readonly float[] AttackSpeed = new float[] { 1.25f, 1.1f, 1f, 0.9f, 0.8f, 0.7f, 0.6f, 0.5f, 0.42f, 0.35f };
    private static readonly float[] MagnetRange = new float[] { 0.2f, 0.8f, 1.2f, 1.6f, 2f, 2.5f, 3f, 3.5f, 4f, 4.3f };
    private static readonly float[] Coins = new float[] { 1f, 1.2f, 1.4f, 1.6f, 1.8f, 2f, 2.2f, 2.4f, 2.6f, 2.8f };
    private static readonly int[] Health = new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
    private static readonly int[] Special = new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

    public static int uDamage = 0;
    public static int uMovementSpeed = 0;
    public static int uAttackType = 0;
    public static int uAttackSpeed = 0;
    public static int uMagnetRange = 0;
    public static int uCoins = 0;
    public static int uHealth = 0;
    public static int uSpecial = 0;

    public static float CoinMultiplier => Coins[uCoins];

    public static float Get(UpgradeType type)
    {
        switch (type)
        {
            default:
            case UpgradeType.Damage:
                return Damage[uDamage];
            case UpgradeType.AttackSpeed:
                return AttackSpeed[uAttackSpeed];
            case UpgradeType.AttackType:
                return AttackType[uAttackType];
            case UpgradeType.Speed:
                return MovementSpeed[uMovementSpeed];
            case UpgradeType.Health:
                return Health[uHealth];
            case UpgradeType.Coins:
                return Coins[uCoins];
            case UpgradeType.Magnet:
                return MagnetRange[uMagnetRange];
            case UpgradeType.Special:
                return Special[uSpecial];
        }
    }
}
