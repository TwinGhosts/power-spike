﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    [SerializeField] private Graphic fadeGraphic = null;
    private Color color;
    private bool isFading = false;

    [HideInInspector] public bool IsFadedIn = false;
    [HideInInspector] public UnityEvent OnFadeOut;
    [HideInInspector] public UnityEvent OnFadeIn;

    private AsyncOperation loadOperation;

    private const float STANDARD_FADE_DURATION = 0.8f;

    private void Awake()
    {
        Util.FadeManager = this;

        fadeGraphic.gameObject.SetActive(true);
        fadeGraphic.color = Color.black;
    }

    // Use this for initialization
    private void Start()
    {
        color = fadeGraphic.color;
        Fade(true);
    }

    private void Update()
    {
        // Test for controlling the loading
        if (loadOperation != null && loadOperation.progress >= 0.9f)
        {
            loadOperation.allowSceneActivation = true;
        }
    }

    private void Fade(bool fadeIn)
    {
        if (!isFading)
            StartCoroutine(_Fade(fadeIn, STANDARD_FADE_DURATION));
    }

    private void Fade(bool fadeIn, float duration = STANDARD_FADE_DURATION)
    {
        if (!isFading)
            StartCoroutine(_Fade(fadeIn, duration));
    }

    #region Scene Switching
    // Reloads the current scene
    public void RestartScene(float duration = STANDARD_FADE_DURATION)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
        }
    }

    public void NextScene()
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut.AddListener(() => LoadAsync(SceneManager.GetActiveScene().buildIndex + 1));
        }
    }

    // Goes to a scene specified with a name
    public void GoToScene(string sceneName)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut.AddListener(() => LoadAsync(sceneName));
        }
    }

    // Goes to a scene specified with an actual scene reference
    public void GoToScene(Scene scene)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut.AddListener(() => LoadAsync(scene.name));
        }
    }

    // Quits the application after fade out
    public void Quit(float duration = STANDARD_FADE_DURATION)
    {
        if (!isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => Application.Quit());
        }
    }
    #endregion Scene Switching

    private void LoadAsync(string sceneName)
    {
        loadOperation = SceneManager.LoadSceneAsync(sceneName);
        loadOperation.allowSceneActivation = false;
    }

    private void LoadAsync(int index)
    {
        loadOperation = SceneManager.LoadSceneAsync(index);
        loadOperation.allowSceneActivation = false;
    }

    // The actual graphic fading
    private IEnumerator _Fade(bool fadeIn, float duration)
    {
        // Variable settings
        isFading = true;

        // Local var setting
        var progress = 0f;
        var startColor = fadeGraphic.color;
        var endColor = color;
        endColor.a = (fadeIn) ? 0f : 1f;

        // Actual fading
        while (progress < 1f)
        {
            progress += Time.deltaTime / duration;
            fadeGraphic.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }

        // Variable resetting
        isFading = false;
        IsFadedIn = fadeIn;
    }
}
