﻿using UnityEngine;

public static class Util
{
    public static readonly int UpgradeLimit = 10;
    public static FadeManager FadeManager;
    public static Player Player;
    public static InGameUI InGameUI;
    public static CameraController CameraController;
    public static GoldInfo GoldInfo;
    public static PrefabHolder PrefabHolder;
    public static BossController BossController;
    public static EntityManager EntityManager;

    public static Bounds stageBounds = new Bounds();
    public static Color White = Color.white;

    public static string SCENE_UPGRADE = "Upgrade";
    public static string SCENE_MAIN_MENU = "Main Menu";
    public static string SCENE_GAME = "Game";

    public static Bounds OrthographicBounds(this Camera camera)
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2;
        Bounds bounds = new Bounds(
            camera.transform.position,
            new Vector3(cameraHeight * screenAspect, cameraHeight, 100f));
        return bounds;
    }
}
