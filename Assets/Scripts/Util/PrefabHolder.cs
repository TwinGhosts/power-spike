﻿using UnityEngine;
using System.Collections;

public class PrefabHolder : MonoBehaviour
{
    public Bullet b1;
    public Bullet b2;
    public Bullet b3;
    public Bullet b4;
    public Bullet b5;
    public Bullet b6;
    public Bullet b7;
    public Bullet b8;
    public Bullet b9;

    private void Awake()
    {
        Util.PrefabHolder = this;
    }
}
