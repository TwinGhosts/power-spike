﻿using System.Collections.Generic;
using UnityEngine;

public class GoldInfo : MonoBehaviour
{
    public Gold gold500;
    public Gold gold100;
    public Gold gold10;
    public Gold gold1;

    private void Awake()
    {
        Util.GoldInfo = this;
    }

    public List<Gold> GetSplitGoldObjects(int value)
    {
        var tempList = new List<Gold>();

        while (value != 0)
        {
            if (value >= 500) { tempList.Add(gold500); value -= 500; }
            else if (value >= 100) { tempList.Add(gold100); value -= 100; }
            else if (value >= 10) { tempList.Add(gold10); value -= 10; }
            else if (value >= 1) { tempList.Add(gold1); value -= 1; }
        }

        return tempList;
    }
}
