﻿using UnityEngine;

public struct ShakeWrapper
{
    public Vector2 Power;
    public float Duration;

    public ShakeWrapper(Vector2 power, float duration)
    {
        Power = power;
        Duration = duration;
    }
}
