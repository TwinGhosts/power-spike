﻿using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 startPosition;
    private List<ShakeWrapper> shakes = new List<ShakeWrapper>();

    private void Awake()
    {
        startPosition = transform.position;
        Util.CameraController = this;
    }

    private void LateUpdate()
    {
        transform.position = startPosition;

        var forces = Vector2.zero;

        if (shakes.Count == 0)
        {
            transform.position = startPosition;
        }
        else
        {
            for (int i = shakes.Count - 1; i >= 0; i--)
            {
                var shaker = shakes[i];
                forces += new Vector2(Random.Range(-shaker.Power.x, shaker.Power.x), Random.Range(-shaker.Power.y, shaker.Power.y));
                shaker.Duration -= Time.deltaTime;
                if (shaker.Duration <= 0f)
                {
                    shakes.Remove(shaker);
                }
            }

            transform.position += (Vector3)forces;
        }
    }

    public void Shake(Vector2 power, float duration)
    {
        shakes.Add(new ShakeWrapper(power, duration));
    }
}
