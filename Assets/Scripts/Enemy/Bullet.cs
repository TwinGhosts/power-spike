﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float angle = 0f;
    public float speed = 0f;
    public int damage = 1;
    public bool rotateTowardsAngle = false;
    public Enemy owner = null;

    [Header("Life Timer")]
    public bool hasLifeTimer = false;
    public float lifeTimer = 10f;

    public SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (hasLifeTimer)
        {
            lifeTimer -= Time.deltaTime;
            if (lifeTimer <= 0f)
            {
                Destroy(gameObject);
            }
        }

        transform.position += (Vector3)MathEx.VectorFromAngle(angle) * Time.deltaTime * speed;
        if (rotateTowardsAngle) transform.rotation = Quaternion.Euler(0f, 0f, angle + 90f);


        if (!Util.stageBounds.Contains(transform.position))
        {
            Destroy(gameObject);
        }
    }

    public void SetColor(Color color)
    {
        spriteRenderer.color = color;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player)
        {
            player.Damage(damage);
            Destroy(gameObject);
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}
