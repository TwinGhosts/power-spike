﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Boss : MonoBehaviour
{
    [SerializeField] protected Transform introPosition;

    [Header("Phases")]
    [SerializeField] protected List<BossPhase> phases = new List<BossPhase>();

    [Header("Particles")]
    [SerializeField] protected ParticleSystem explosionSystem;
    [SerializeField] protected ParticleSystem hitSystem;

    [Header("Gold")]
    [SerializeField] protected Vector2Int goldAmountPerHitRange;
    [SerializeField] protected int goldAmountOnDeath;

    [Header("State")]
    [SerializeField] protected State state;
    [SerializeField] protected float introTime = 4f;
    [SerializeField] protected float dieTime = 3f;

    [Title("References")]
    [SerializeField] protected SpriteRenderer spriteRenderer;

    public BossPhase CurrentPhase => currentPhase;
    public List<BossPhase> Phases => phases;
    public int CurrentPhaseIndex => currentPhaseIndex;

    protected Color startColor;
    protected BossPhase currentPhase;
    protected int currentPhaseIndex;

    public Action<float> OnHit = (damage) => { };
    public Action<BossPhase> OnNextPhase = (nextPhase) => { };

    public float TotalHealth
    {
        get
        {
            var health = 0f;
            foreach (var phase in phases)
            {
                health = phase.MaxHealth;
            }

            return health;
        }
    }

    protected enum State
    {
        Intro,
        Active,
        Dead,
    }

    protected IEnumerator Start()
    {
        if (!phases.Any())
            throw new Exception($"'{name}' has no phases attached to it!");

        startColor = spriteRenderer.color;
        currentPhaseIndex = phases.Count - 1;
        currentPhase = phases[currentPhaseIndex];

        state = State.Intro;
        Util.InGameUI.SetBoss(this);

        var startPosition = transform.position;
        var progress = 0f;
        while (progress < 1f)
        {
            transform.position = Vector2.Lerp(startPosition, introPosition.position, Mathf.SmoothStep(0f, 1f, progress));
            progress += Time.deltaTime / introTime;
            yield return null;
        }

        state = State.Active;
    }

    protected void Update()
    {
        switch (state)
        {
            case State.Intro:
                break;

            case State.Active:
                if (spriteRenderer.color != startColor)
                    spriteRenderer.color = Color.Lerp(spriteRenderer.color, startColor, Time.deltaTime * 4f);
                Patterns();
                break;

            case State.Dead:
                transform.position += (Vector3)Vector2.down * 0.5f * Time.deltaTime;
                dieTime -= Time.deltaTime;

                var targetColor = Color.white;
                targetColor.a = 0f;
                spriteRenderer.color = Color.Lerp(spriteRenderer.color, targetColor, Time.deltaTime * 2f);

                if (dieTime < 0.1f) Util.InGameUI.FlashScreen();
                if (dieTime <= 0f)
                {
                    Destroy(gameObject);
                }
                break;
        }
    }

    public void Damage(float damage)
    {
        if (state != State.Active) return;

        currentPhase.Health = Mathf.Clamp(currentPhase.Health - damage, 0f, TotalHealth);
        Hit();
        OnHit.Invoke(damage);

        if (currentPhase.Health <= 0f)
        {
            currentPhaseIndex--;
            if (currentPhaseIndex < 0)
                Die();
            else
                currentPhase = phases[currentPhaseIndex];

            Debug.Log(currentPhase.Health);
        }
    }

    public void Die()
    {
        SpawnGold(false);
        state = State.Dead;
        explosionSystem.gameObject.transform.SetParent(null);
        explosionSystem.Play();
        Util.CameraController.Shake(Vector2.one * 0.25f, dieTime);
        Util.EntityManager.KillAllHostileBullets();
    }

    protected virtual void Patterns() { }

    protected void Hit()
    {
        spriteRenderer.color = Color.red;
    }

    public void SpawnGold(bool moveToPlayer = false)
    {
        foreach (var gold in Util.GoldInfo.GetSplitGoldObjects(goldAmountOnDeath))
        {
            var offset = 2f;
            var goldObject = Instantiate(gold, transform.position, Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(0, 360f)));
            goldObject.Spread((Vector2)transform.position + UnityEngine.Random.insideUnitCircle * offset);
            goldObject.MoveToPlayer = moveToPlayer;
        }
    }
}
