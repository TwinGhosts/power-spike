﻿using System;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float maxHealth = 100f;
    public int goldSpawnAmount = 1;
    public State state = State.ACTIVE;

    protected float health;
    protected SpriteRenderer spriteRenderer;
    protected Color startColor;

    public float Health => health;

    public Action<float> OnHit = (damage) => { };
    public Action OnDeath = () => { };

    protected virtual void Awake()
    {
        health = maxHealth;
        spriteRenderer = GetComponent<SpriteRenderer>();
        startColor = spriteRenderer.color;
    }

    protected virtual void Start()
    {
        Util.EntityManager.Enemies.Add(this);
    }

    protected virtual void Update()
    {
        if (spriteRenderer.color != startColor)
            spriteRenderer.color = Color.Lerp(spriteRenderer.color, startColor, Time.deltaTime * 4f);
    }

    public virtual void Damage(float damage)
    {
        if (state != State.ACTIVE) return;

        health = Mathf.Clamp(health - damage, 0f, maxHealth);
        OnHit.Invoke(damage);
        Hit();

        if (health <= float.Epsilon)
        {
            Die();
        }
    }

    protected virtual void Hit()
    {
        spriteRenderer.color = Color.red;
    }

    public void SpawnGold(bool moveToPlayer = false)
    {
        foreach (var gold in Util.GoldInfo.GetSplitGoldObjects(goldSpawnAmount))
        {
            var offset = 2f;
            var goldObject = Instantiate(gold, transform.position, Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(0, 360f)));
            goldObject.Spread((Vector2)transform.position + UnityEngine.Random.insideUnitCircle * offset);
            goldObject.MoveToPlayer = moveToPlayer;
        }
    }

    public virtual void Die()
    {
        OnDeath.Invoke();
        SpawnGold();
        Destroy(gameObject);
    }

    public enum State
    {
        INTRO,
        ACTIVE,
        DEAD,
    }

    private void OnDestroy()
    {
        Util.EntityManager.Enemies.Remove(this);
    }
}
