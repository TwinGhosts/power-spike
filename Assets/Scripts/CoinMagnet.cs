﻿using UnityEngine;
using System.Collections;

public class CoinMagnet : MonoBehaviour
{
    private CircleCollider2D col;

    private void Awake()
    {
        col = GetComponent<CircleCollider2D>();
        col.radius = Stats.Get(UpgradeType.Magnet);
    }
}
